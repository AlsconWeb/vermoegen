<?php get_header(); ?>
	<article id="mvp-article-wrap" <?php post_class(); ?> itemscope itemtype="http://schema.org/NewsArticle">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div id="mvp-article-cont" class="left relative">
				<div class="mvp-main-box">
					<div id="mvp-post-main" class="left relative">
						<header id="mvp-post-head" class="left relative">
							<h1 class="mvp-post-title left entry-title" itemprop="headline"><?php the_title(); ?></h1>
						</header>
						<div class="mvp-post-main-out left relative">
							<div class="mvp-post-main-in">
								<div id="mvp-post-content" class="left relative">
									<div id="mvp-content-wrap" class="left relative">
										<div class="mvp-post-soc-out right relative">
											<?php $socialbox = get_option( 'mvp_social_box' );
											if ( $socialbox == "true" ) { ?>
												<?php $socialbox = get_option( 'mvp_social_box' );
												if ( $socialbox == "true" ) { ?>
													<?php if ( function_exists( 'mvp_SocialSharing' ) ) { ?>
														<?php mvp_SocialSharing(); ?>
													<?php } ?>
												<?php } ?>
											<?php } ?>
											<div class="mvp-post-soc-in">
												<div id="mvp-content-body" class="left-relative">
													<div id="mvp-content-main" class="left relative">
														<?php the_content(); ?>
														<?php wp_link_pages(); ?>
													</div><!--mvp-content-main-->
													<div id="mvp-content-bot" class="left relative">
														<div class="mvp-org-wrap" itemprop="publisher" itemscope
														     itemtype="https://schema.org/Organization">
															<div class="mvp-org-logo" itemprop="logo" itemscope
															     itemtype="https://schema.org/ImageObject">
																<?php if ( get_option( 'mvp_logo' ) ) { ?>
																	<img src="<?php echo esc_url( get_option( 'mvp_logo' ) ); ?>"/>
																	<meta itemprop="url" content="<?php echo esc_url( get_option( 'mvp_logo' ) ); ?>">
																<?php } else { ?>
																	<img src="<?php echo get_template_directory_uri(); ?>/images/logos/logo-nav.png"
																	     alt="<?php bloginfo( 'name' ); ?>"/>
																	<meta itemprop="url"
																	      content="<?php echo get_template_directory_uri(); ?>/images/logos/logo-nav.png">
																<?php } ?>
															</div><!--mvp-org-logo-->
															<meta itemprop="name" content="<?php bloginfo( 'name' ); ?>">
														</div><!--mvp-org-wrap-->

														<?php if ( comments_open() ) { ?>
															<?php $disqus_id = get_option( 'mvp_disqus_id' );
															if ( $disqus_id ) {
																if ( isset( $disqus_id ) ) { ?>
																	<div id="mvp-comments-button" class="left relative mvp-com-click">
																		<span
																			class="mvp-comment-but-text"><?php comments_number( __( 'Comments', 'zox-news' ), esc_html__( 'Comments', 'zox-news' ), esc_html__( 'Comments', 'zox-news' ) ); ?></span>
																	</div><!--mvp-comments-button-->
																	<?php $disqus_id2 = esc_html( $disqus_id );
																	mvp_disqus_embed( $disqus_id2 ); ?>
																<?php }
															} else { ?>
																<?php $mvp_click_id = get_the_ID(); ?>
																<div id="mvp-comments-button" class="left relative mvp-com-click">
																	<span
																		class="mvp-comment-but-text"><?php comments_number( __( 'Click to comment', 'zox-news' ), esc_html__( '1 Comment', 'zox-news' ), esc_html__( '% Comments', 'zox-news' ) ); ?></span>
																</div><!--mvp-comments-button-->
																<?php comments_template(); ?>
															<?php } ?>
														<?php } ?>
													</div><!--mvp-content-bot-->
												</div><!--mvp-content-body-->
											</div><!--mvp-post-soc-in-->
										</div><!--mvp-post-soc-out-->
									</div><!--mvp-content-wrap-->
								</div><!--mvp-post-content-->
							</div><!--mvp-post-main-in-->
							<?php get_sidebar(); ?>
						</div><!--mvp-post-main-out-->
						<?php if ( is_page( [ 4601 ] ) ): ?>
							<div id="mvp-post-more-wrap" class="left relative">
								<h4 class="mvp-widget-home-title">
																	<span
																		class="mvp-widget-home-title"><?php echo esc_html( get_option( 'mvp_pop_head' ) ); ?></span>
								</h4>
								<ul class="mvp-post-more-list left relative">
									<?php
									$uIP         = wp_unslash( $_SERVER['REMOTE_ADDR'] );
									$celebrityID = get_transient( 'iwp_' . str_replace( '.', '', $uIP ) );
									$recent      = new WP_Query(
										[
											'posts_per_page'      => 8,
											'ignore_sticky_posts' => 1,
											'post__not_in'        => [ $celebrityID ],
											'orderby'             => 'meta_value_num',
											'order'               => 'DESC',
											'meta_query'          => [
												[
													'key'     => 'post_views_count',
													'compare' => 'EXISTS',
												],
											],
										]
									);
									while ( $recent->have_posts() ) :
										$recent->the_post();
										?>
										<a href="<?php the_permalink(); ?>" rel="bookmark">
											<li>
												<?php if ( ( function_exists( 'has_post_thumbnail' ) ) && ( has_post_thumbnail() ) ) { ?>
													<div class="mvp-post-more-img left relative">
														<?php the_post_thumbnail( 'mvp-mid-thumb', [ 'class' => 'mvp-reg-img' ] ); ?>
														<?php the_post_thumbnail( 'mvp-small-thumb', [ 'class' => 'mvp-mob-img' ] ); ?>
														<?php if ( has_post_format( 'video' ) ) { ?>
															<div class="mvp-vid-box-wrap mvp-vid-box-mid mvp-vid-marg">
																<i class="fa fa-2 fa-play" aria-hidden="true"></i>
															</div><!--mvp-vid-box-wrap-->
														<?php } else if ( has_post_format( 'gallery' ) ) { ?>
															<div class="mvp-vid-box-wrap mvp-vid-box-mid">
																<i class="fa fa-2 fa-camera" aria-hidden="true"></i>
															</div><!--mvp-vid-box-wrap-->
														<?php } ?>
													</div><!--mvp-post-more-img-->
												<?php } ?>
												<div class="mvp-post-more-text left relative">
													<div class="mvp-cat-date-wrap left relative">
										<span class="mvp-cd-cat left relative">
										<?php
										$category = get_the_category();
										echo esc_html( $category[0]->cat_name );
										?>
										</span><span
															class="mvp-cd-date left relative"><?php printf( esc_html__( '%s ago', 'zox-news' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
													</div><!--mvp-cat-date-wrap-->
													<p><?php the_title(); ?></p>
												</div><!--mvp-post-more-text-->
											</li>
										</a>
									<?php
									endwhile;
									wp_reset_postdata();
									?>
								</ul>
							</div><!--mvp-post-more-wrap-->
						<?php endif; ?>
					</div><!--mvp-post-main-->
				</div><!--mvp-main-box-->
			</div><!--mvp-article-cont-->
		<?php endwhile; endif; ?>

	</article><!--mvp-article-wrap-->

<?php get_footer(); ?>