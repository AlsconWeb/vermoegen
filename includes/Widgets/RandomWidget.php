<?php
/**
 * Created 30.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Widgets
 */

namespace IWP\Widgets;

/**
 * Class RandomWidget
 *
 * @package IWP\Widgets
 */
class RandomWidget extends \WP_Widget {
	/**
	 * RandomWidget constructor.
	 */
	public function __construct() {
		$widget_options = [
		'classname'   => 'random_post_widget',
		'description' => __( 'This widget redirects to a random article', 'zox-news-child' ),
		];
		parent::__construct( 'random_post_widget', __( 'Random Post', 'zox-news-child' ), $widget_options );
	}
	
	/**
	 * Output widget in widget zone.
	 *
	 * @param array $args     Arguments.
	 * @param array $instance Instance.
	 */
	public function widget( $args, $instance ) {
		$title      = apply_filters( 'widget_title', $instance['title'] );
		$textButton = $instance['text_button'];
		?>
		<?php if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ) :
			echo $args['before_widget'] . $args['before_title'] . $title . $args['after_title']; ?>
			<form action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post">
				<input type="hidden" name="action" value="random">
				<button class="button random-button" id="random-btn-widget" type="submit">
					Zufalls-Promi
					<amp-img
					src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/dice-five-solid.svg"
					width="100"
					height="100"
					layout="fixed"
					alt="dice-five"
					></amp-img>
				</button>
			</form>
			<?php echo $args['after_widget']; ?>
		<?php else : echo $args['before_widget'] . $args['before_title'] . $title . $args['after_title']; ?>
			<div class="button-wrapp">
				<a class="button random-button" href="#">
					<?php echo esc_html( $textButton ); ?>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/dice-five-solid.svg"
					alt="dice-five">
				</a>
			</div>
			<?php echo $args['after_widget']; endif;
	}

	/**
	 * From Widget.
	 *
	 * @param array $instance Instence.
	 *
	 * @return string|void
	 */
	public function form( $instance ) {
		$title      = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$textButton = ! empty( $instance['text_button'] ) ? $instance['text_button'] : '';
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>"
			name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'text_button' ); ?>">Text Button</label>
			<input type="text" id="<?php echo $this->get_field_id( 'text_button' ); ?>"
			name="<?php echo $this->get_field_name( 'text_button' ); ?>" value="<?php echo esc_attr( $textButton ); ?>"/>
		</p>
		<?php
	}
	
	/**
	 * Save data widget.
	 *
	 * @param array $new_instance New Instance.
	 * @param array $old_instance Old Instance.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance                = $old_instance;
		$instance['title']       = strip_tags( $new_instance['title'] );
		$instance['text_button'] = strip_tags( $new_instance['text_button'] );
		
		return $instance;
	}
}
