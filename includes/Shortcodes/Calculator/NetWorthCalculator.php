<?php
/**
 * Created 30.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Shortcodes\Calculator
 */

namespace IWP\Shortcodes\Calculator;

/**
 * Class NetWorthCalculator.
 *
 * @package IWP\Shortcodes\Calculator
 */
class NetWorthCalculator {
	/**
	 * Connect to DB class.
	 *
	 * @var \QM_DB|\wpdb
	 */
	private $db;

	/**
	 * NetWorthCalculator constructor.
	 */
	public function __construct () {
		add_shortcode ( 'net_worth_calculator', [ $this, 'outputShortCode' ] );
		add_shortcode ( 'net_worth_calculator_result', [ $this, 'outputShortCodeResult' ] );
		add_filter ( 'calculator_before_content', [ $this, 'calculatorBeforeContent' ], 10, 3 );

		add_action ( 'admin_post_nopriv_calculation', [ $this, 'calculatedHandler' ] );
		add_action ( 'admin_post_calculation', [ $this, 'calculatedHandler' ] );

		global $wpdb;
		$this->db = $wpdb;
	}

	/**
	 * Output Calculator HTML Code.
	 *
	 * @param mixed  $atts    Attributes.
	 * @param string $content Content.
	 *
	 * @return false|string
	 */
	public function outputShortCode ( $atts = null, $content = null ) {
		ob_start ();
		include get_stylesheet_directory () . '/template/shortcode/calculator/template-calculator.php';

		return ob_get_clean ();
	}

	/**
	 * Output Calculator Answer HTML Code.
	 *
	 * @param null|mixed  $atts    Attributes.
	 * @param null|string $content Content.
	 *
	 * @return false|string
	 */
	public function outputShortCodeResult ( $atts = null, $content = null ) {
		ob_start ();
		include get_stylesheet_directory () . '/template/shortcode/calculator/template-result.php';

		return ob_get_clean ();
	}

	/**
	 * Filter before output content.
	 * Replaces the value of keys with pedals.
	 *
	 * ###Filter Name : calculator_before_content
	 *
	 *
	 * ###Available Values: {{celebrity}}, {{net_worth_celebrity}}, {{users_net_worth}}, {{users_monthly_savings}}
	 *
	 * @param string     $content Content.
	 * @param int|string $postID  Post ID.
	 * @param string     $key     The key to be replaced.
	 *
	 * @return string
	 */
	public function calculatorBeforeContent ( string $content, $postID, string $key ): string {

		switch ( $key ) {
			case '{{celebrity}}':
				$content = str_replace ( $key, $this->getCelebrityName ( (int) $postID ), $content );
				break;
			case '{{net_worth_celebrity}}':
				$content = str_replace ( $key, number_format ( $this->getNetWorthCelebrity ( (int) $postID ), '0', ',', '.' ), $content );
				break;
			case '{{users_net_worth}}':
				$usersNetWorth = ! empty( $_COOKIE['uCurrentActive'] ) ? filter_var ( wp_unslash ( $_COOKIE['uCurrentActive'] ), FILTER_SANITIZE_STRING ) : '0';
				$content       = str_replace ( $key, number_format ( $usersNetWorth, '0', ',', '.' ), $content );
				break;
			case '{{users_monthly_savings}}':
				$usersMonthlySavings = ! empty( $_COOKIE['uSavePerMonth'] ) ? filter_var ( wp_unslash ( $_COOKIE['uSavePerMonth'] ), FILTER_SANITIZE_STRING ) : '0';
				$content             = str_replace ( $key, number_format ( $usersMonthlySavings, '0', ',', '.' ), $content );
				break;
		}

		return $content;
	}

	/**
	 * Get Celebrity Name.
	 *
	 * @param int $postID Post ID.
	 *
	 * @return string
	 */
	private function getCelebrityName ( int $postID ): string {
		return str_replace ( 'Vermögen', '', get_the_title ( $postID ) );
	}

	/**
	 * Get Net Worth of Celebrities.
	 *
	 * @param int $postID Post ID.
	 *
	 * @return string
	 */
	private function getNetWorthCelebrity ( int $postID ): string {
		$netWorth = '0';
		if ( function_exists ( 'get_field' ) ) {
			$netWorth = get_field ( 'v-net-worth-amount', $postID );
		}

		if ( empty( $netWorth ) ) {
			return '';
		}

		return $netWorth;
	}

	/**
	 * Handler Calculator Form.
	 */
	public function calculatedHandler (): void {
		if ( empty( $_POST ) || ! wp_verify_nonce ( wp_unslash ( $_POST['calculation_nonce_field'] ), 'calculation' ) ) {
			wp_safe_redirect ( $_POST['_wp_http_referer'] . '?error=not+valid+nonce', 301 );
			exit;
		}

		$uCurrentActive = ! empty( $_POST['currentActive'] ) ? (int) filter_var (
			(int) $_POST['currentActive'],
			FILTER_SANITIZE_NUMBER_FLOAT
		) : 0;

		$uSavePerMonth = ! empty( $_POST['savingsPerMonth'] ) ? (int) filter_var (
			(int) $_POST['savingsPerMonth'],
			FILTER_SANITIZE_NUMBER_FLOAT
		) : 0;

		$uIP         = ! empty( $_POST['ip_user'] ) ? filter_var ( wp_unslash ( $_POST['ip_user'] ), FILTER_VALIDATE_IP ) : null;
		$uAgent      = ! empty( $_POST['user_agent'] ) ? filter_var ( wp_unslash ( $_POST['user_agent'] ), FILTER_SANITIZE_STRING ) : null;
		$celebrityID = ! empty( $_POST['celebrityID'] ) ? (int) filter_var ( wp_unslash ( $_POST['celebrityID'] ), FILTER_SANITIZE_NUMBER_FLOAT ) : 0;

		set_transient ( 'iwp_' . str_replace ( '.', '', $uIP ), $celebrityID, HOUR_IN_SECONDS );

		if ( $uSavePerMonth && $uCurrentActive && $celebrityID ) {
			$years = $this->calculatedResult ( $uCurrentActive, $uSavePerMonth, $celebrityID, 'year' );
			$month = $this->calculatedResult ( $uCurrentActive, $uSavePerMonth, $celebrityID, 'month' );

			if ( false !== $years && false !== $month ) {

				$addStat = $this->addStatisticCalculation (
					$uIP,
					$uAgent,
					$uCurrentActive,
					$uSavePerMonth,
					[
						'year'  => $years,
						'month' => $month,
					],
					$celebrityID
				);
				setcookie ( 'userYears', $years, ( time () + 3600 ), '/' );
				setcookie ( 'userMonth', $month, ( time () + 3600 ), '/' );
				setcookie ( 'uCurrentActive', $uCurrentActive, ( time () + 3600 ), '/' );
				setcookie ( 'uSavePerMonth', $uSavePerMonth, ( time () + 3600 ), '/' );
				$pageRedirectUrl = get_option ( 'iwp_answer_page', true );
				wp_safe_redirect ( get_the_permalink ( $pageRedirectUrl ), 301 );
				exit;
			}
		}

		wp_safe_redirect ( $_POST['_wp_http_referer'] . '?error=not+valid+field&errors=true', 301 );
		exit;
	}

	/**
	 * ##Calculate Results by Year Or Month
	 *
	 * ###Calculation keys available: year | month
	 *
	 * @param int    $uCurrentActive User's starting capital.
	 * @param int    $uSavePerMonth  How much does a user save each month.
	 * @param int    $celebrityID    Post ID Celebrity.
	 * @param string $key            The key by which the calculation will be: year | month.
	 *
	 * @return float
	 */
	public function calculatedResult ( int $uCurrentActive, int $uSavePerMonth, int $celebrityID, string $key ): float {
		$celebrityActive = (float) $this->getNetWorthCelebrity ( $celebrityID );
		switch ( $key ) {
			case 'year':
				$result = round ( ( ( ( $celebrityActive - $uCurrentActive ) / $uSavePerMonth ) / 12 ), 3 );
				break;
			case 'month':
				$result = round ( ( ( $celebrityActive - $uCurrentActive ) / $uSavePerMonth ), 3 );
				break;
			default:
				$result = false;
		}

		if ( $result < 0 ) {
			$result = 0;
		}

		return $result;
	}

	/**
	 * Add Row in table iwp_calculator_statistic.
	 *
	 * @param string $uIP            IP User.
	 * @param string $uAgent         User Agent.
	 * @param float  $uCurrentActive Current User Active.
	 * @param float  $uSavePerMonth  Money  Save per Month.
	 * @param array  $result         Result calculation.
	 * @param int    $celebrityID    Post ID.
	 *
	 * @return bool
	 */
	public function addStatisticCalculation (
		string $uIP, string $uAgent, float $uCurrentActive, float $uSavePerMonth, array $result,
		int $celebrityID
	):
	bool {

		$prefix = $this->db->base_prefix;

		$addRow = $this->db->insert (
			$prefix . 'iwp_calculator_statistic',
			[
				'active'   => $uCurrentActive,
				'saving'   => $uSavePerMonth,
				'result'   => wp_json_encode ( $result ),
				'uip'      => $uIP,
				'postName' => get_the_title ( $celebrityID ),
				'uagent'   => $uAgent,
				'postID'   => $celebrityID,
			],
			[
				'%f',
				'%f',
				'%s',
				'%s',
				'%s',
				'%s',
				'%d',
			]
		);

		if ( $addRow ) {
			return true;
		}

		return false;
	}
}
