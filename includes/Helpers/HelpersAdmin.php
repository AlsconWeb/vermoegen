<?php
/**
 * Created 03.08.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Helpers
 */

namespace IWP\Helpers;

/**
 * Class HelpersAdmin.
 */
class HelpersAdmin {
	/**
	 * Connect to DB Class.
	 *
	 * @var \QM_DB|\wpdb
	 */
	private $db;
	/**
	 * Exel generate Class.
	 *
	 * @var \SimpleXLSXGen
	 */
	private $exel;

	/**
	 * Constructor HelpersAdmin.
	 */
	public function __construct() {
		global $wpdb;

		$this->db   = $wpdb;
		$this->exel = new \SimpleXLSXGen();
	}

	/**
	 * Query to the iwp_calculator_statistic table
	 *
	 * @param int      $countLimit Count Limit Query.
	 * @param int|null $offset     Offset Query.
	 * @param array    $argsSort   Arguments by sort.
	 *
	 * @return array|false|object
	 */
	public function getStatisticTable( int $countLimit, int $offset = null, array $argsSort = null ) {
		$prefix = $this->db->base_prefix;
		$sql    = '';
		if ( null === $offset || $offset === 1 ) {
			$sortString = '';
			if ( null !== $argsSort ) {
				$sortString = 'ORDER BY `' . $argsSort['sortBy'] . '` ' . $argsSort['order'];
			}
			$sql = "SELECT * FROM `{$prefix}iwp_calculator_statistic`" . $sortString . ' LIMIT ' . $countLimit;
		}

		if ( $offset && 1 !== $offset ) {
			if ( null !== $argsSort ) {
				$sortString = 'ORDER BY `' . $argsSort['sortBy'] . '` ' . $argsSort['order'];
			}
			$offsetCount = ( $offset * $countLimit ) - $countLimit;
			$sql         = "SELECT * FROM `{$prefix}iwp_calculator_statistic`" . $sortString . ' LIMIT ' . $countLimit . ' OFFSET ' . $offsetCount;
		}

		$response = $this->db->get_results( $sql, OBJECT );

		if ( empty( $response ) ) {
			return false;
		}

		return $response;
	}

	/**
	 * Number of pages available for output.
	 *
	 * @param int $countOutput Number of lines displayed per page.
	 *
	 * @return false|float
	 */
	public function getCountPage( int $countOutput ) {
		$sql    = "SELECT COUNT(`id`) FROM `{$this->db->prefix}iwp_calculator_statistic`  WHERE 1";
		$result = (int) $this->db->get_results( $sql, ARRAY_N )[0][0];

		return ceil( $result / $countOutput );
	}

	/**
	 * Create EXEL File.
	 *
	 * @return bool
	 */
	public function generateExelFile(): bool {
		$data = [
			[
				'ID',
				'Celebrity Page',
				'User Active',
				'User Save per Month',
				'Result',
				'User IP',
				'User Agent',
				'Date',
			],
		];

		$listDocs = $this->getStatisticTable( 100000000 );
		foreach ( $listDocs as $doc ) {
			$result        = json_decode( $doc->result );
			$month         = $result->month;
			$year          = $result->year;
			$resultString  = "Monate: $month Jahre: $year";
			$celebrityPage = '<a href="' . get_the_permalink( $doc->postID ) . '">' . get_the_title( $doc->postID ) . '</a>';

			$data[] = [
				$doc->id,
				$celebrityPage,
				$doc->active,
				$doc->saving,
				$resultString,
				$doc->uip,
				$doc->uagent,
				$doc->date,
			];
		}
		$baseDir = wp_upload_dir();

		if ( wp_mkdir_p( $baseDir['basedir'] . '/export' ) ) {
			$file = $baseDir['basedir'] . '/export';
		}

		if ( is_writable( $baseDir['basedir'] . '/export' ) ) {
			$xlsx = $this->exel->fromArray( $data )->saveAs(
				$baseDir['basedir'] . '/export/Statistic-reports-' . current_time( 'Y-m-d' ) .
				'.xlsx'
			);
		} else {
			return false;
		}

		return $xlsx;
	}
}
