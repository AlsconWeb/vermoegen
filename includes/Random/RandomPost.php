<?php
/**
 * Created 29.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package Randome
 */

namespace IWP\Random;

use WP_Query;

/**
 * Class RandomPost
 *
 * @package IWP\Random
 */
class RandomPost {
	/**
	 * RandomPost constructor.
	 */
	public function __construct() {
		add_action( 'wp_ajax_get_random', [ $this, 'randomAjax' ] );
		add_action( 'wp_ajax_nopriv_get_random', [ $this, 'randomAjax' ] );

		// add script and style.
		add_action( 'wp_enqueue_scripts', [ $this, 'addScripts' ] );

		add_action( 'admin_post_random', [ $this, 'randomPost' ] );
		add_action( 'admin_post_nopriv_random', [ $this, 'randomPost' ] );
	}

	/**
	 * Ajax Handler Random Post.
	 *
	 * Return url random post.
	 */
	public function randomAjax(): void {
		$randomPostUrl = $this->randomPostUrl();

		if ( ! empty( $randomPostUrl ) ) {
			wp_send_json_success( [ 'url' => $randomPostUrl ] );
		}

		wp_send_json_error( [ 'message' => esc_html__( 'Posts not found', 'zox-news' ) ] );
	}

	/**
	 * Add Scripts and Style.
	 */
	public function addScripts(): void {
		wp_enqueue_script(
			'iwp-random',
			get_stylesheet_directory_uri() . '/assets/js/random.js',
			[ 'jquery' ],
			'1.0.0',
			true
		);

		wp_localize_script(
			'iwp-random',
			'iwp',
			[
				'url' => admin_url( 'admin-ajax.php' ),
			]
		);

	}

	/**
	 * Get random post url.
	 *
	 * @return false|string
	 */
	private function randomPostUrl() {
		$arg = [
			'posts_per_page' => 1,
			'post_type'      => 'post',
			'orderby'        => 'rand',
		];

		$query = new WP_Query( $arg );
		if ( ! $query->have_posts() ) {
			return false;
		}

		$randomID = $query->posts[0]->ID;

		return get_the_permalink( $randomID );
	}

	/**
	 * Redirect to Random Post on AMP Version.
	 */
	public function randomPost(): void {
		$randomUrl = $this->randomPostUrl();
		header( 'AMP-Redirect-To: ' . $randomUrl . '/amp/' );
		header( 'Access-Control-Expose-Headers: AMP-Redirect-To' );
		die();
	}
}
