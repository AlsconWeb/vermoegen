<?php
/**
 * Created 02.08.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Init
 */

namespace IWP\Init;

use IWP\Helpers\HelpersAdmin;
use IWP\MetaBox\MetaBoxPageSettings;

/**
 * Class Init.
 *
 * @package IWP\Init
 */
class Init {
	/**
	 * Connect to DB.
	 *
	 * @var \wpdb
	 */
	private $db;
	/**
	 * Admin helpers class.
	 *
	 * @var \IWP\Helpers\HelpersAdmin
	 */
	private $helpers;

	/**
	 * Constructor Init.
	 */
	public function __construct() {
		global $wpdb;

		$this->db = $wpdb;
		$this->createTable();
		$this->addAnswerPage();
		$this->helpers = new HelpersAdmin();

		add_action( 'admin_menu', [ $this, 'addAdminMenu' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'addAdminStyleAndScripts' ] );
		add_action( 'wp_ajax_exel_generate', [ $this, 'generateExel' ] );
		add_action( 'wp_ajax_sort_table', [ $this, 'sortTable' ] );

		add_filter( 'the_content', [ $this, 'addShortCodeToPost' ], 10, 1 );
		add_filter( 'get_comment_author', [ $this, 'changeAuthorName' ], 10, 1 );
		new MetaBoxPageSettings();
	}

	/**
	 * Create Table Statistic from calculator.
	 */
	public function createTable(): void {
		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		$prefix = $this->db->base_prefix;
		$rating = $this->db->get_results( "SHOW TABLES LIKE '{$prefix}iwp_calculator_statistic'" );
		if ( ! $rating ) {
			$charset_collate = "DEFAULT CHARACTER SET {$this->db->charset} COLLATE {$this->db->collate}";
			$sql             = "CREATE TABLE `{$prefix}iwp_calculator_statistic` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `active` INT NULL , `saving` INT NULL , `result` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL , `uip` TEXT  CHARACTER SET utf8 COLLATE utf8_general_ci NULL , `uagent` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL , `postName` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL, `date` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `postID` INT NULL , PRIMARY KEY (`id`)) {$charset_collate};";

			dbDelta( $sql );
		}
	}

	/**
	 * Create Answer Page from calculator
	 */
	public function addAnswerPage(): void {

		$page = get_option( 'iwp_answer_page', false );

		if ( ! $page ) {
			$conetnt   = '[net_worth_calculator_result tiitle="Ergebnis der Berechnung:"]Um so reich zu werden wie {{celebrity}} ({{net_worth_celebrity}} Euro) benötigst Du bei einem jetzigen Vermögen von {{users_net_worth}} und einer monatlichen Sparrate von {{users_monthly_savings}} insgesamt:[/net_worth_calculator_result]';
			$post_data = [
				'post_title'   => sanitize_title( 'Testergebnis' ),
				'post_content' => $conetnt,
				'post_status'  => 'publish',
				'post_author'  => 1,
				'post_type'    => 'page',
			];
			$post_id   = wp_insert_post( $post_data );

			if ( ! is_wp_error( $post_id ) ) {
				add_option( 'iwp_answer_page', $post_id, '', 'yes' );
			}
		}

	}

	/**
	 * Add to Admin menu statistic page.
	 */
	public function addAdminMenu(): void {
		add_menu_page(
			esc_html__( 'Statistic Page', 'zox-news' ),
			esc_html__( 'Statistic', 'zox-news' ),
			'edit_others_posts',
			'iwp-calculator-statistic',
			[
				$this,
				'outputStatisticPage',
			],
			'dashicons-chart-area',
			30
		);
	}

	/**
	 * Output Html Statistic Page.
	 */
	public function outputStatisticPage(): void {
		ob_start();
		include_once get_stylesheet_directory() . '/template/admin/statistic-page.php';
		echo ob_get_clean();
	}

	/**
	 * Add Style and Scripts to admin page.
	 *
	 * @param string $hook Hook Name Page.
	 */
	public function addAdminStyleAndScripts( string $hook ): void {

		if ( 'toplevel_page_iwp-calculator-statistic' === $hook ) {
			wp_enqueue_style( 'bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css', '', '5.0.2', 'all' );
			wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/assets/js/admin/main.js', [
				'jquery',
				'sweetalert2',
			], '1.0.0', true );
			wp_enqueue_script( 'sweetalert2', '//cdn.jsdelivr.net/npm/sweetalert2@11.0.12/dist/sweetalert2.all.min.js', [ 'jquery' ], '', true );
			wp_localize_script(
				'main',
				'iwp',
				[
					'ajaxURL' => admin_url( 'admin-ajax.php' ),
				]
			);

			wp_enqueue_style( 'fontawesome', get_stylesheet_directory_uri() . '/assets/css/all.min.css', '', '6.0.0', 'all' );
		}
	}

	/**
	 * Ajax Handler exel_generate.
	 */
	public function generateExel(): void {
		$generateFile = $this->helpers->generateExelFile();

		if ( $generateFile ) {
			$baseUrl = wp_upload_dir()['baseurl'] . '/export/';
			$url     = $baseUrl . 'Statistic-reports-' . current_time( 'Y-m-d' ) . '.xlsx';
			wp_send_json_success( [ 'url' => $url ] );
		}

		wp_send_json_error( [ 'file_generate' => $generateFile ] );
	}

	/**
	 * Add ShotCode to all posts.
	 *
	 * @param string $content Content Post.
	 *
	 * @return string
	 */
	public function addShortCodeToPost( string $content ): string {
		global $post;

		$outputCalc = get_post_meta( $post->ID, 'iwp_page_settings_calc', true );
		if ( ! $outputCalc ) {
			$outputCalc = 'on';
		}

		if ( 'post' === $post->post_type && 'on' === $outputCalc ) {
			$shorcode = '[net_worth_calculator]Teste jetzt wie lange Du brauchst, um so reich zu werden wie {{celebrity}}:[/net_worth_calculator]';
			$shorcode = apply_filters( 'calculator_before_content', $shorcode, $post->ID, '{{celebrity}}' );

			return $content . '<br>' . $shorcode;
		}

		return $content;
	}

	/**
	 * Sort table statistic.
	 */
	public function sortTable(): void {
		$field = isset( $_POST['field'] ) ? filter_var( wp_unslash( $_POST['field'] ), FILTER_SANITIZE_STRING ) : NULL;
		$sort  = isset( $_POST['sort'] ) ? filter_var( wp_unslash( $_POST['sort'] ), FILTER_SANITIZE_STRING ) : NULL;

		if ( NULL === $field || NULL === $sort ) {
			wp_send_json_error( [ 'message' => __( 'Sorting is not selected or the data came incorrectly', 'zox-news' ) ] );
		}

		$sortTableData = $this->helpers->getStatisticTable( 200, NULL, [ 'sortBy' => $field, 'order' => $sort ] );

		if ( ! empty( $sortTableData ) ) {
			ob_start();
			include get_stylesheet_directory() . '/template/admin/statistic-page-ajax.php';
			wp_send_json_success( [ 'html' => ob_get_clean() ] );
		}
	}

	/**
	 * Changes the Anonymous to a German equivalent
	 *
	 * @param mixed $author Author Name.
	 *
	 * @return mixed|string
	 */
	public function changeAuthorName( $author ) {
		if ( empty( $author ) || 'Anonymous' === $author ) {
			return 'BESUCHER';
		}

		return $author;
	}
}
