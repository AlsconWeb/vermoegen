jQuery(document).ready(function ($) {
	
	let paramsUrl = location.search;
	let params = new URLSearchParams(paramsUrl);
	let error = params.get('error');
	
	if (error) {
		console.log('IF')
		Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Es müssen beide Felder ausgefüllt werden!',
		}).then((result) => {
			console.log('then')
			if (result.isConfirmed) {
				location.href = $('[name=iwp_to_redirect]').val();
			}
		});
	}
	
	$('button.fa-search').click(function (e) {
		e.preventDefault();
		if ($(this).hasClass('open')) {
			$(this).removeClass('open')
			$('#mvp-main-nav-wrap').css('padding-bottom', '0px');
			$('#search_form_input').hide();
		} else {
			$(this).addClass('open')
			$('#mvp-main-nav-wrap').css('padding-bottom', '50px');
			$('#search_form_input').show();
		}
	})
	let button = $('.random-button');
	if (button.length) {
		$(button).click(function (e) {
			e.preventDefault();
			
			let data = {
				action: 'get_random',
			}
			
			$.ajax({
				type: 'POST',
				url: iwp.url,
				data: data,
				success: function (res) {
					if (res.success) {
						console.log(res.data.url)
						location.href = res.data.url;
					}
					
					if (!res.success) {
						alert(res.data.message);
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log('error...', xhr);
					//error logging
				},
			});
		})
	}
});