jQuery(document).ready(function ($) {
	/**
	 * Generate Exel File
	 */
	$('.generate-exel').click(function (e) {
		e.preventDefault();
		
		let data = {
			action: 'exel_generate'
		}
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxURL,
			data: data,
			success: function (res) {
				if (res.success) {
					Swal.fire({
						title: '<strong>Download File</strong>',
						icon: 'info',
						html:
							'You can Download File, ' +
							`<a href="${res.data.url}">links</a> `,
						showCloseButton: true,
						focusConfirm: false,
						confirmButtonText:
							'<i class="fa fa-thumbs-up"></i> Great!',
						confirmButtonAriaLabel: 'Thumbs up, great!',
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			},
		});
	});
	
	/**
	 * Sort dy columns
	 */
	$('.fas.fa-chevron-up, .fas.fa-chevron-down').click(function (e) {
		let data = {
			action: 'sort_table',
			field: $(this).data('item'),
			sort: $(this).data('action')
		}
		
		$.ajax({
			type: 'POST',
			url: iwp.ajaxURL,
			data: data,
			success: function (res) {
				console.log(res);
				if (res.success) {
					$('.table.table-striped tbody').remove();
					$('.table.table-striped').append(res.data.html);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			}
		});
		console.log(data);
	});
	
	$('.fas.fa-chevron-up').click(function (e) {
		$(this).hide();
		$(this).parent().find('.fas.fa-chevron-down').show();
	});
	
	$('.fas.fa-chevron-down').click(function (e) {
		$(this).hide();
		$(this).parent().find('.fas.fa-chevron-up').show();
	});
	
});