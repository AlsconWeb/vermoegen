<?php
/**
 * Created 03.08.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP
 */

use IWP\Helpers\HelpersAdmin;

$countOutput   = 200;
$helpers       = new HelpersAdmin();
$sortTableData = $helpers->getStatisticTable( $countOutput, (int) isset( $_GET['pages'] ) ? $_GET['pages'] : null );

?>
<h1><?php echo esc_html__( 'Statistic', 'zox-news' ); ?></h1>
<div class="row">
	<div class="col">
		<button type="button" class="btn btn-secondary generate-exel">
			<?php
			echo esc_html__(
			'Generate EXEL',
			'zox-news'
			);
			?>
		</button>
	</div>
</div>
<div class="table-responsive">
	<table class="table table-striped">
		<thead>
		<tr>
			<th scope="col">
				<?php echo esc_html( 'ID' ); ?>
				<i class="fas fa-chevron-down" data-action="desc" data-item="id"></i>
				<i class="fas fa-chevron-up" data-action="asc" data-item="id" style="display: none;"></i>
			</th>
			<th scope="col">
				<?php echo esc_html( 'Celebrity Page' ); ?>
				<i class="fas fa-chevron-down" data-action="desc" data-item="postName"></i>
				<i class="fas fa-chevron-up" data-action="asc" data-item="postName" style="display: none;"></i>
			</th>
			<th scope="col">
				<?php echo esc_html( 'User Active' ); ?>
				<i class="fas fa-chevron-down" data-action="desc" data-item="active"></i>
				<i class="fas fa-chevron-up" data-action="asc" data-item="active" style="display: none;"></i>
			</th>
			<th scope="col">
				<?php echo esc_html( 'User Save per Month' ); ?>
				<i class="fas fa-chevron-down" data-action="desc" data-item="saving"></i>
				<i class="fas fa-chevron-up" data-action="asc" data-item="saving" style="display: none;"></i>
			</th>
			<th scope="col"><?php echo esc_html( 'Result' ); ?></th>
			<th scope="col">
				<?php echo esc_html( 'User IP' ); ?>
				<i class="fas fa-chevron-down" data-action="desc" data-item="uip"></i>
				<i class="fas fa-chevron-up" data-action="asc" data-item="uip" style="display: none;"></i>
			</th>
			<th scope="col">
				<?php echo esc_html( 'User Agent' ); ?>
				<i class="fas fa-chevron-down" data-action="desc" data-item="uagent"></i>
				<i class="fas fa-chevron-up" data-action="asc" data-item="uagent" style="display: none;"></i>
			</th>
			<th scope="col">
				<?php echo esc_html( 'Date' ); ?>
				<i class="fas fa-chevron-down" data-action="desc" data-item="date"></i>
				<i class="fas fa-chevron-up" data-action="asc" data-item="date" style="display: none;"></i>
			</th>
		</tr>
		</thead>
		<tbody>
		<?php if ( $sortTableData ) : ?>
			<?php
			foreach ( $sortTableData as $data ) :
				$result = json_decode( $data->result );
				$month = $result->month;
				$yearOnUser = $result->year;
				?>
				<tr>
					<th scope="row"><?php echo esc_html( $data->id ); ?></th>
					<td>
						<?php if ( ! empty( $data->postID ) ) : ?>
							<a href="<?php echo esc_url( get_the_permalink( $data->postID ) ); ?>">
								<?php
								echo esc_html( get_the_title( $data->postID ) );
								?>
							</a>
						<?php endif; ?>
					</td>
					<td><?php echo esc_html( $data->active ); ?></td>
					<td><?php echo esc_html( $data->saving ); ?></td>
					<td>
						<p>Monate: <?php echo esc_html( $month ); ?></p>
						<p>Jahre: <?php echo esc_html( $yearOnUser ); ?></p>
					</td>
					<td><?php echo esc_html( $data->uip ); ?></td>
					<td><?php echo esc_html( $data->uagent ); ?></td>
					<td><?php echo esc_html( $data->date ); ?></td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		</tbody>
	</table>
</div>
<nav>
	<?php $numberPage = $helpers->getCountPage( $countOutput ); ?>
	<?php if ( $numberPage ) : ?>
		<ul class="pagination">
			<?php for ( $i = 0; $i < $numberPage; $i ++ ) : ?>
				<li class="page-item">
					<a class="page-link"
					href="/wp-admin/admin.php?page=iwp-calculator-statistic&pages=<?php echo esc_attr( $i + 1 ); ?>"><?php echo esc_attr( $i + 1 ); ?></a>
				</li>
			<?php endfor; ?>
		</ul>
	<?php endif; ?>
</nav>
