<?php
/**
 * Created 13.08.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */
?>
<tbody>
<?php if ( $sortTableData ) : ?>
	<?php
	foreach ( $sortTableData as $data ) :
		$result = json_decode( $data->result );
		$month = $result->month;
		$yearOnUser = $result->year;
		?>
		<tr>
			<th scope="row"><?php echo esc_html( $data->id ); ?></th>
			<td>
				<?php if ( ! empty( $data->postID ) ) : ?>
					<a href="<?php echo esc_url( get_the_permalink( $data->postID ) ); ?>">
						<?php
						echo esc_html( get_the_title( $data->postID ) );
						?>
					</a>
				<?php endif; ?>
			</td>
			<td><?php echo esc_html( $data->active ); ?></td>
			<td><?php echo esc_html( $data->saving ); ?></td>
			<td>
				<p>Monate: <?php echo esc_html( $month ); ?></p>
				<p>Jahre: <?php echo esc_html( $yearOnUser ); ?></p>
			</td>
			<td><?php echo esc_html( $data->uip ); ?></td>
			<td><?php echo esc_html( $data->uagent ); ?></td>
			<td><?php echo esc_html( $data->date ); ?></td>
		</tr>
	<?php endforeach; ?>
<?php endif; ?>
</tbody>
