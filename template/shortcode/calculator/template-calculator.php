<?php
/**
 * Created 30.07.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Shortcodes\Calculator
 */

$remote_addr = ! empty( $_SERVER['REMOTE_ADDR'] ) ? filter_var( wp_unslash( $_SERVER['REMOTE_ADDR'] ), FILTER_SANITIZE_STRING ) : '';
$user_agent  = ! empty( $_SERVER['HTTP_USER_AGENT'] ) ? filter_var( wp_unslash( $_SERVER['HTTP_USER_AGENT'] ), FILTER_SANITIZE_STRING ) : '';


$atts = shortcode_atts(
[
'text_current_active'    => 'Dein jetziges Vermögen',
'text_savings_per_month' => 'Dein monatlicher Sparbetrag',
'text_button'            => 'Jetzt berechnen',
'currency'               => 'Euro',
],
$atts
);
global $post;
?>
<div class="calculator-wrapper">
	<h4><?php echo esc_html( $content ); ?></h4>
	<div class="calculator">
		<form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
			<?php if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() && ! empty( $_GET['errors'] ) ) : ?>
				<p class="red" style="color: red">Es müssen beide Felder ausgefüllt werden!</p>
			<?php endif; ?>
			<div class="mb-3">
				<label for="currentActive" class="form-label"><?php echo esc_html( $atts['text_current_active'] ); ?></label>
				<input type="text" class="form-control" name="currentActive" id="currentActive">
				<span><?php echo esc_html( $atts['currency'] ); ?></span>
			</div>
			<div class="mb-3">
				<label for="savingsPerMonth" class="form-label"> <?php echo esc_html( $atts['text_savings_per_month'] ); ?></label>
				<input type="text" class="form-control" name="savingsPerMonth" id="savingsPerMonth">
				<span><?php echo esc_html( $atts['currency'] ); ?></span>
			</div>
			<div class="mb-3">
				<input type="hidden" name="action" value="calculation">
				<input type="hidden" name="iwp_to_redirect" value="<?php the_permalink(); ?>">
				<input type="hidden" name="ip_user" value="<?php echo esc_attr( $remote_addr ); ?>">
				<input type="hidden" name="user_agent" value="<?php echo esc_attr( $user_agent ); ?>">
				<input type="hidden" name="celebrityID" value="<?php echo esc_attr( $post->ID ); ?>">
				<?php wp_nonce_field( 'calculation', 'calculation_nonce_field' ); ?>
				<input type="submit" class="button button-green" value="<?php echo esc_html( $atts['text_button'] ); ?>">
			</div>
		</form>
	</div>
</div>
