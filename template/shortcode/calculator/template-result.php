<?php
/**
 * Created 02.08.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Shortcodes\Calculator
 */

$atts = shortcode_atts (
[
'title' => 'Ergebnis der Berechnung:',
],
$atts
);

$keys = [
'{{celebrity}}',
'{{net_worth_celebrity}}',
'{{users_net_worth}}',
'{{users_monthly_savings}}',
];

$year_result  = ! empty( $_COOKIE['userYears'] ) ? filter_var ( wp_unslash ( $_COOKIE['userYears'] ), FILTER_SANITIZE_STRING ) : 'Wert nicht berechnet';
$month_result = ! empty( $_COOKIE['userMonth'] ) ? filter_var ( wp_unslash ( $_COOKIE['userMonth'] ), FILTER_SANITIZE_STRING ) : 'Wert nicht berechnet';
$uIP          = wp_unslash ( $_SERVER['REMOTE_ADDR'] );
$year_result  = number_format ( $year_result, '0', ',', '.' );
$month_result = number_format ( $month_result, '0', ',', '.' );

$celebrityID = get_transient ( 'iwp_' . str_replace ( '.', '', $uIP ) );
foreach ( $keys as $key ) {
	$content = apply_filters ( 'calculator_before_content', $content, $celebrityID, $key );
}
?>
<div class="result-wrapper">
	<p><?php echo esc_html ( $content ); ?></p>
	<div class="year-result">
		<p><strong><?php echo ( 0 !== $year_result ? esc_html ( $year_result ) : '0' ); ?></strong> Jahre</p>
	</div>
	<div class="month-result">
		<p>bzw. <span><strong><?php echo ( 0 !== $month_result ? esc_html ( $month_result ) : '0' ); ?></strong> Monate</span></p>
	</div>
</div>


